const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

// 保存 webpack 的所有配置信息
module.exports = {

    // 配置环境，'development' or 'production'
    mode: "development",

    // 入口文件
    entry: "./src/index.ts",

    // 输出信息
    output: {
        // 路径
        path: path.resolve(__dirname, "dist"),
        // 生成文件名
        filename: "bundle.js",
        // 不要使用箭头函数，IE 不兼容（了解即可）
        // environment: {
        //     arrowFunction: false,
        // }
    },
    
    // 指定打包时需要用到的模块
    module: {
        // 指定加载规则
        rules: [
            // 设置 ts 文件的处理规则
            {
                // 匹配哪些文件
                test: /\.ts$/,
                // 要使用的loader，多个时从后往前依次执行
                use: [
                    // 配置 Babel
                    {
                        loader: "babel-loader",
                        options: {
                            // 设置预定义的环境
                            presets: [
                                [
                                    // 指定环境的插件
                                    "@babel/preset-env",
                                    // 配置信息
                                    {
                                        // 要兼容的目标浏览器
                                        targets: {
                                            "chrome": "88",
                                            // "ie": "11"
                                        },
                                        // corejs 可以模拟 js 运行环境，使得老的浏览器也可以使用新的 JS 功能
                                        // 指定 corejs 的版本
                                        "corejs": "3",
                                        // 指定使用 corejs 的方式，usage 按需加载
                                        "useBuiltIns": "usage"
                                    }
                                ]
                            ]
                        }
                    },
                    'ts-loader'
                ],
                // 要排除的文件
                exclude: /node_modules/
            }, 
            // 设置 less 文件的处理规则
            {
                test: /\.less$/,
                use: [
                    "style-loader",
                    "css-loader",
                    // 引入 postcss
                    {
                        loader: "postcss-loader",
                        options: {
                            postcssOptions: {
                                plugins: [
                                    [
                                        "postcss-preset-env",
                                        {
                                            // 兼容最新两个版本的浏览器
                                            browsers: 'last 2 versions'
                                        }
                                    ]
                                ]
                            }
                        }
                    },
                    "less-loader"
                ]
            },
        ]
    },

    // 指定哪些文件可以作为模块被引入
    resolve: {
        extensions: ['.ts', '.js']
    },

    // 加载插件及其配置信息
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: "./src/index.html"
        }),
    ],
}